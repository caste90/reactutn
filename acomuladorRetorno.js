import React from 'react';
import styles from './Css/Css';
import Retorno from './Componentes/Retorno';
import {Text, View, Button, } from 'react-native';


export default class acomuladorRetorno extends React.Component {
    constructor(props) {
        super(props);
    } 
    showSaved() {
        var aux = this.props.list;
        if (aux.length !== 0) {
            return aux.map((value, index) =>
                <Retorno liftUpNum={this.props.liftUpNum.bind(this, value.numeros)} key={index} list={value.numeros}/>
            );
        }
         return (
             <Text style={styles.texto}>Aún no hay totales cargados</Text>
             );

    }
    render() {

        return (
            <View style={styles.container}>
                <Text style={styles.title}>TOTALES</Text>
                
                {this.showSaved()}
                
                <View style={styles.containerBotones}>
                    <Button title="AGREGAR CONTADORES"  color="#0292f9" onPress={this.props.function} />
                    <Button title="LIMPIAR ALMACENADOS" color="#f90202" onPress={this.props.eliminarTodo} />
                    
                </View>
            </View>
            );
    }
}


