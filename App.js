import React from 'react';
import { View, AsyncStorage} from 'react-native';
import styles from './Css/Css';
import AcomuladorRetorno from './acomuladorRetorno';
import Contadores from './Contadores';
import { createStackNavigator } from 'react-navigation'; 
import store from './store';

class Ppal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            list: []
        };
        store.subscribe(() => {
            this.setState({
                list:store.getState().list
            });
            this.save();
          }
        );
    }

   //actualizo num en store y re-dirijo hacia Programa
   _Programa(dato) {
        store.dispatch({
            type: "updateNum",
            numeros: dato
        });
        this.props.navigation.navigate('Programa');
    }


    

    deleteAll() {
        store.dispatch({
            type: "clean_store"
        });
    }

    
    add() {
        store.dispatch({
            type: "alternate",
            cambiar: false
        });
        this.props.navigation.navigate('Programa');
    }

    
    save() {
        AsyncStorage.setItem('xxxx', JSON.stringify(store.getState().list));
    }

    
    render() {
        return (
            <View style={styles.container}>
                <AcomuladorRetorno list={this.state.list} eliminarTodo={this.deleteAll.bind(this)} liftUpNum={this._Programa.bind(this)} function={this.add.bind(this)} />
            </View>
        );
    }
}

export default createStackNavigator(
    {
        Principal: {
            screen: Ppal,
            navigationOptions: {
                title: 'Inicio - TP React UTN FRLP'
            }
        },
        Programa: {
            screen: Contadores,
            navigationOptions: {
                title: 'Volver'
            }
        }
    },
    {
        initialRouteName: 'Principal'
    }

);


