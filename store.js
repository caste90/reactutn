import { createStore } from "redux";

const reducer = (state, action) => {

    if (action.type === "liftList") {
        return {
            ...state,
            list: action.statusNow
        };
    }
    
    else if (action.type === "updateNum") {
        return {
            ...state,
            valueNow: action.numeros,
            cambio: true
        };
    }
    
    else if (action.type === "deleteNum") {
        return {
            ...state,
            valueNow: [0]
        };
    } 
    
    else if (action.type === "clean_store") {
        return {
            ...state,
            list: []
        };
    } 

    else if (action.type === "alternate") {
        return {
            ...state,
            cambio: action.cambio
        };
    }
    else if (action.type === "loadList") {
        return {
            ...state,
            list: state.list.concat(action.statusNow)
        };
    } 
    return state;
};

export default createStore(reducer, { list: [], valueNow:[0],cambio:false});