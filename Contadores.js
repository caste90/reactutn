import React from 'react';
import Numero from './Componentes/ValoresContenedor';
import store from './store';
import styles from './Css/Css';
import { Text, View, Button, TouchableOpacity, StyleSheet } from 'react-native';


export default class Contadores extends React.Component {
    constructor() {
        super();

        var varSecundaria = store.getState().valueNow;

        this.state = {
            numeros: varSecundaria
        };
        
        cambio = store.getState().cambio;
    }

    //recorro
    forechNum() {
        return this.state.numeros.map((data,index) => {
            return (
                
                <Numero key={index} number={data} NumAdd={this.NumAdd.bind(this, index)} NumDisc={this.NumDisc.bind(this, index)} />
                
            );
        });
    }

    //sumo contenido vector
    sumar(previousValue , currentValue) {
        return previousValue + currentValue;
    }


    //sumo numeros de a dos
    NumAdd(index) {
        this.setState(previousState => {
            var varSecundaria = this.state.numeros;
            varSecundaria[index] = varSecundaria[index] + 2;
            return {
                numeros: varSecundaria
            };
        });
    }

    //resto numero unitariamente
    NumDisc(index) {
        this.setState(previousState => {
            var varSecundaria = this.state.numeros;
            varSecundaria[index] = varSecundaria[index] - 1;
            return {
                numeros: varSecundaria
            };
        });
    }

     //agrego contador. Max 4
     addCounter() {
        var varSecundaria = this.state.numeros;
        if (varSecundaria.length !== 4) {
            varSecundaria.push(0);
            this.setState({
                numeros: varSecundaria
            });
        }
    }

    //saco contador.
    subtractCounter() {
        var varSecundaria = this.state.numeros;
        if (varSecundaria.length!==1) {
            varSecundaria.pop();
            this.setState({
                numeros: varSecundaria
            });
        }
      
    }

    //Almaceno la lista
    saveStore() {
        store.dispatch({
            type: "deleteNum"
        });
        if (!cambio) {
            store.dispatch({
                type: "loadList",
                statusNow: this.state
            });
        }
        this.props.navigation.navigate("Principal");
    }

    
    



    

    render() {
        return (
            <View style={styles.container}>

                <Text style={styles.title}>NUEVO TOTAL</Text>
               
                <View style={styless.container2}>
                    <TouchableOpacity style={styless.button2} onPress={this.addCounter.bind(this)} >
                         <Text style={styless.textoo}> Agregar contador </Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={styless.button2} onPress={this.subtractCounter.bind(this)} >
                         <Text style={styless.textoo}> Eliminar contador </Text>
                    </TouchableOpacity>
                 </View>


                
                {this.forechNum()}
                

                <View style={styles.sumatoria}>
                    <Text style={{ fontSize: 15 }}>Sumatoria:</Text>

                    <View style={[styles.borde, { backgroundColor: '#ffedb5' }]}>
                        <Text style={[styles.title, { backgroundColor: '#ffedb5' }]}>{this.state.numeros.reduce(this.sumar)}</Text>
                    </View>

                    <Button style={styles.botones} title="Guardar" color="#bababa" onPress={() => this.saveStore()} />
                </View>
                
            </View>
        );
}
}




const styless = StyleSheet.create({
    container2: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-around',
      backgroundColor: '#4A6075',
    },
  
    button2: {
      backgroundColor: '#2c3e50',
      padding: 15,
    },
  
    textoo: {
      color: '#fff',
      fontWeight: '700',
    },
  });