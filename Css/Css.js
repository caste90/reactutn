import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    
    title: {
        fontSize: 40,
        backgroundColor: '#ffffff',
        alignSelf: 'center'
    },
    resultado: {
        fontSize: 40,
        backgroundColor: '#ffffff',
        alignSelf: 'center'
    },
    texto2: {
        fontSize: 22,
        height: 300,
        flexDirection: 'column',
        alignSelf: 'center',
        alignItems: 'center'
    },
    texto: {
        fontSize: 20,
        alignSelf: 'center'
    },
    containerBotones: {
        width: 360,
        height: 120,
        alignSelf: 'center',
        flexDirection: 'column',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    botones: {
        width: 300
    },
    contadores: {
        flexDirection: 'row',
        justifyContent: 'space-around'

    },
    numeroContainer: {
        marginLeft: 20,
        marginRight: 20,
        padding:2,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    bottonNumero: {
        borderWidth: 1.5,
    },

    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#ffffff',
        justifyContent: 'space-around'
    },

    borde: {
        borderWidth: 1
    },
    sumatoria: {
        flex: 2,
        flexDirection: 'column',
        justifyContent: 'flex-end'
    },
    returnContent: {
        flexDirection: 'column'
    }
});