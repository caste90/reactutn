import React from 'react';
import { Text, View} from 'react-native';
import styles from '../Css/Css';


export default class Retorno extends React.Component {

    //sumo contenido vector
    sumar(previousValue , currentValue , indice, vector) {
        return previousValue + currentValue;
    }

    render() {
        return (
            
                <View style={styles.returnContent}>

                    <View onPress={this.props.CargarNumeros}>  

                        
                        <Text>{new Date().getMonth() + ' Nov ' + new Date().getUTCFullYear() + ' - ' + new Date().getHours() + 'Hs'}</Text>
                        

                        <View style={[styles.borde]}>
                                <Text style={styles.resultado}>{this.props.list.reduce(this.sumar)}</Text>
                        </View>

                    </View> 
                    
                </View>
            
        );
    }



}





