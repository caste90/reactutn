import React from 'react';
import styles from '../Css/Css';
import {Text, View, StyleSheet, TouchableOpacity, ScrollView,  } from 'react-native';


export default class ValoresContenedor extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
    
            <ScrollView style={styless.scrollViewContainer}>
                <View>


                    <View style={styless.container}>
                                
                                <View style={styless.buttonsContainer}>
                                        <TouchableOpacity style={styless.buttonAdd} onPress={this.props.NumAdd} >
                                            <Text style={styless.icon}>+</Text>
                                        </TouchableOpacity>
                                </View>
        
                                <Text style={styless.counter}>{this.props.number}</Text>
        
                                <View style={styless.buttonsContainer}>        
                                    <TouchableOpacity style={styless.buttonRemove} onPress={this.props.NumDisc } >
                                        <Text style={styless.icon}>-</Text>
                                    </TouchableOpacity>
                                </View>
        
        
                        </View>

                </View>
            </ScrollView>
                
            

        );
    }
}

const styless = StyleSheet.create({
  
    buttonsContainer: {
      flexDirection: 'row',
    },

    container: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: '#eee',
        borderBottomWidth: 2,
        borderColor: '#e1e1e1',
        marginVertical: 10,
    },
  
    buttonAdd: {
      backgroundColor: '#2ecc71',
      paddingVertical: 15,
      paddingHorizontal: 35,
    },
  
    buttonRemove: {
      backgroundColor: '#e74c3c',
      paddingVertical: 15,
      paddingHorizontal: 35,
    },

    icon: {
        color: '#2c3e50',
        fontSize: 45,
    },

    counter: {
        flex: 1,
        color: '#2c3e50',
        fontSize: 45,
        fontWeight: '700',
        textAlign: 'center',
    },
    scrollViewContainer: {
        flex: 1,
    },

    
  });